# Telegrambots
Collection of my Telegrambots.
<br>
<br>
## [Smart Assistance Bot](https://code.toold.xyz/ThomasKoscheck/Telegrambots/tree/master/smart%20assistance%20bot)
This is a machine learning conversational bot with some additional features of an assistance bot.

## [Homebot](https://code.toold.xyz/ThomasKoscheck/Telegrambots/tree/master/homebot)
Telegrambot to control your server or smarthome

## [MotivationBot](https://code.toold.xyz/ThomasKoscheck/Telegrambots/tree/master/motivationbot)
Telegrambot that answers on any text with motivational quotes


